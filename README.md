# HSPC Patient Picker

## Setup

HSPC Patient Picker is hosted as a static web app.

## Hosting
````
npm install
npm run serve
````

The app is available at http://localhost:8094.
